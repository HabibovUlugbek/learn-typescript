class Vehicle {
    // color :string;
    // constructor(color:string) {
    //     this.color = color
        
    // }
    constructor(public color:string){}
    start ():void {
        console.log("it`s class")
    }
    stop():void {
        console.log("It's end of the class")
    }
    protected middleWare():void {
        console.log("I`m on the way")
    }
}

class Bus extends Vehicle {
    constructor(public isAirBag:boolean, color:string){
        super(color)
    }
    private stopDrive():void{
        console.log('Its bus class')
    }
    DriveProcess():void {
        this.stopDrive();
        this.middleWare();
    }
}

const myVehicle = new Vehicle("yellow");
console.log(myVehicle.color)
myVehicle.start()
myVehicle.stop();

const myBus = new Bus(false,"blue");
console.log(myBus.color)
console.log(myBus.isAirBag)
myBus.DriveProcess();
