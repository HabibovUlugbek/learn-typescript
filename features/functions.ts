const calculate  = (a:number, b:number):number => {
    return a*b;
}

function divide(a:number, b:number) :number{
    return a/b
}

//void and never type
const logger = (msg:string):void => {
    console.log(msg)
}
const err = (err:string) :void => {
    if(!err){
        throw new Error(err);
    }
    // return err; xato qaytariladi
}

//Object 

const vehicle = {
    name:"Audi",
    isAirBag:false,
}

const loggerCar = ({name, isAirBag}:{name:string, isAirBag:boolean}):void => {
    console.log(`name:${name} 
                Airbag : ${isAirBag}`)
}

loggerCar(vehicle);

const mers = {
    color: "black",
    isAirBag:true,
    locations:{
        country:"German",
        city:"Bayern",
    },
    setColor(color:string):void{
        this.color = color;
    },
}

const {color}:{color:string} = mers;
const {locations :{country, city}} :{locations:{country:string, city:string}}= mers;
