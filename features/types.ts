const now = new Date(); // type date

const people = {
    age:20,
}

class Habibov {
    
}
const habibov = new Habibov();

let oranges:number= 8;
//  oranges = "habibov" chunki orangew type number bu annotations 

//Annotation

let number:number = 23;
let names:string = "Habiboov";
let hasCar:boolean = false;
let not:null = null;
let undf:undefined = undefined;

// function , array , object
//ARRAY
const fruit:string[] = ['apple','watermelon',"peach","banana"]
const num: number[] = [23,345,567,6];
const isMerried: boolean[] = [true, false, false];
//OBJECT
const car :{name:string,color:string, isAirbag:boolean} = {
    name:"BMW",
    color: "Black",
    isAirbag:false
}
//ClASS
class Car {

} 
const Audi:Car = new Car();

//FUNCTIONS
const error = (err:string) => {
    console.log(err);
}
error("Not found")

//ANY

//When to use type annotations

//1- functions returns any type 

const objJSON = '{"x":10,"y":8}';
const coordinate:{x:number,y:number} = JSON.parse(objJSON)

//2 - When we declare a variable on one line and initialize later 
const numbers = ["one", "two","six"];
let isCurrent:boolean;

for (let i = 0; i < numbers.length; i++) {
    if(numbers[i] === "one"){
        isCurrent = true;
    }
}

// 3- Variable whose type can not be inferred

const testNumber = [-56,-456,78]
let positive:boolean | number = false;
for (let i = 0; i < testNumber.length; i++) {
    if(testNumber[i]>0) positive=testNumber[i];
    
}