interface PublicCar {
    name:string,
    color:string,
    wheels:number,
    speed():number,
}

const mashina:PublicCar = {
    color:"red",
    name:"nexia",
    wheels:4,
    speed():number {
        return 320
    }
}

const myCar = (car: PublicCar):void => {
 console.log(`Name: ${car.name}
            Speed : ${car.speed()}`)
}

myCar(mashina)