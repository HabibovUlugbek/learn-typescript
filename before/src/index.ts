//Basic types
let id: number = 5;
let company: string = "Innovators"
let isPublished: boolean = true
let x: any = "hello"

let ids: number[]= [1,3,5,6,8]

//Tuple
let person: [number , string, boolean] = [23,"salomlar", true]
//tuple array 
let employee: [number, string][]

employee = [
    [1,'Brad'],
    [2,"john"]
]

//Union
let uuid: string | number = 22

//Enum
enum Direction1{
    Up = 12,
    Down = 23,
    Left = 214,
    Right = 5352
}

// Object

type User = {
    id:number,
    name : string
}

const user: User= {
    id :1,
    name : "John",

}

// Type Assertion
let cid : any = 1
// let customerId = <number>cid
let customerId = cid as number


// Functions
function addNumber(x: number, y: number) : number{
    return x + y;
}

function log(message:string | number): void {
    console.log(message)
}

//Interfaces

interface UserInterface {
    readonly id:number,
    name:string
    age?:number
}

 const user1: UserInterface = {
     id: 1,
     name: 'John '
 }

interface MathFunc {
    (x:number, y:number):number
}

const add:MathFunc = (x:number , y:number):number => x+y
const sub:MathFunc = (x:number , y:number):number => x-y


interface PersonInterface {
    id:number,
    name:string,
    register() :string
}

//Classes
class Person implements PersonInterface {
    id: number 
    name: string
     
    constructor(id: number , name:string){
        this.id = id,
        this.name = name
    }

    register(){
        return `${this.name} is now registered`
    }
}

const brad = new Person(1, 'Brad')
const mike = new Person(3, "Mike tyson")


//Subclasses
class Employee extends Person{
    position: string

    constructor(id: number, name: string ,position: string){
        super(id, name )
        this.position = position
    }
}

//Generycs
function getArray <T>(items:T[]):T[] {
    return new Array().concat(items)
}

let numArray = getArray<number>([13,5,643,347])
let stringArray = getArray<string>(['asf','asdf','ada','safa'])
stringArray.push('salom')

const programmer =new Employee(22,'Habibov', 'devops')

console.log(programmer.register())