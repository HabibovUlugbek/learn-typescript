"use strict";
//Basic types
let id = 5;
let company = "Innovators";
let isPublished = true;
let x = "hello";
let ids = [1, 3, 5, 6, 8];
//Tuple
let person = [23, "salomlar", true];
//tuple array 
let employee;
employee = [
    [1, 'Brad'],
    [2, "john"]
];
//Union
let uuid = 22;
//Enum
var Direction1;
(function (Direction1) {
    Direction1[Direction1["Up"] = 12] = "Up";
    Direction1[Direction1["Down"] = 23] = "Down";
    Direction1[Direction1["Left"] = 214] = "Left";
    Direction1[Direction1["Right"] = 5352] = "Right";
})(Direction1 || (Direction1 = {}));
const user = {
    id: 1,
    name: "John",
};
// Type Assertion
let cid = 1;
// let customerId = <number>cid
let customerId = cid;
// Functions
function addNumber(x, y) {
    return x + y;
}
function log(message) {
    console.log(message);
}
const user1 = {
    id: 1,
    name: 'John '
};
const add = (x, y) => x + y;
const sub = (x, y) => x - y;
//Classes
class Person {
    constructor(id, name) {
        this.id = id,
            this.name = name;
    }
    register() {
        return `${this.name} is now registered`;
    }
}
const brad = new Person(1, 'Brad');
const mike = new Person(3, "Mike tyson");
//Subclasses
class Employee extends Person {
    constructor(id, name, position) {
        super(id, name);
        this.position = position;
    }
}
//Generycs
function getArray(items) {
    return new Array().concat(items);
}
let numArray = getArray([13, 5, 643, 347]);
let stringArray = getArray(['asf', 'asdf', 'ada', 'safa']);
stringArray.push('salom');
const programmer = new Employee(22, 'Habibov', 'devops');
console.log(programmer.register());
