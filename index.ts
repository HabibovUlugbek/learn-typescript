import axios from "axios";

const url ="https://jsonplaceholder.typicode.com/todos/1";

interface User {
    id:number,
    title:string,
    completed:boolean
}
const logger = (id:number, title:string, completed:boolean) => {
    console.log("UserId:", id);
    console.log("Title: ", title);
    console.log("MyStatus:",completed);
}

axios.get(url).then(res=> {
    const todo = res.data as User;
    const ID = todo.id;
    const Title = todo.title
    const Status = todo.completed
    logger(ID,Title,Status);
})