"use strict";
exports.__esModule = true;
var axios_1 = require("axios");
var url = "https://jsonplaceholder.typicode.com/todos/1";
var logger = function (id, title, completed) {
    console.log("UserId:", id);
    console.log("Title: ", title);
    console.log("MyStatus:", completed);
};
axios_1["default"].get(url).then(function (res) {
    var todo = res.data;
    var ID = todo.id;
    var Title = todo.title;
    var Status = todo.completed;
    logger(ID, Title, Status);
});
